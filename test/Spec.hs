{-# LANGUAGE OverloadedStrings #-}
import Test.Tasty
import Test.Tasty.HUnit

import Data.Aeson (encode)
import Database.MySQL.Simple
       (connect, query_, execute_, ConnectInfo(..), defaultConnectInfo)
import Data.Attoparsec.ByteString (parseOnly, parseTest)
import Data.Either (isRight)

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC8

import MySQLdb
import SFTP

main :: IO ()
main = defaultMain tests


tests :: TestTree
tests = testGroup "Tests" [unitTests, parserTests]

instance Show LMSLogRow

unitTests =
    testCaseSteps "MySQLdb tests" $
    \step ->
         do step "Preparing..."
            conn <-
                connect
                    (defaultConnectInfo
                     { connectHost = "192.168.99.100"
                     , connectPort = 32769
                     , connectDatabase = "snadb"
                     , connectPassword = "secret"
                     })
            step "Create table"
            execute_
                conn
                "CREATE TABLE IF NOT EXISTS system_log (id INT(10), users_ID MEDIUMINT(8), timestamp DOUBLE, type VARCHAR(20), title TEXT, content TEXT, event_type VARCHAR(255), courses_ID MEDIUMINT(8), lessons_ID MEDIUMINT(8), sessions_ID MEDIUMINT(8), branches_ID MEDIUMINT(8))"
            step "Fill db"
            execute_
                conn
                "INSERT INTO system_log (id,users_ID,timestamp,type,event_type) VALUES(7277,20,1460535418.7154,'action','test_updated')"
            xs <-
                query_ conn "SELECT * FROM system_log WHERE id = 7277" :: IO [LMSLogRow]
            assertFailure
                (show (encode (head xs)))


parserTests =
    testCaseSteps "Nginx logs parser tests" $
    \step ->
         do step "Prepearing...read logs from file"
            content <- B.readFile "test/nginx.log"
            step "Start testing"
            mapM_ (\line -> parseTest lmsNginxLogRow line) (BC8.lines content)
            mapM_
                (\line ->
                      assertBool
                          (show (parseOnly lmsNginxLogRow line))
                          (isRight (parseOnly lmsNginxLogRow line)))
                (BC8.lines content)
