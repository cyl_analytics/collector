{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
module MySQLdb (grabFromMySQLdb, LMSLogRow(..)) where

import Numeric (showFFloat)
import Data.Maybe (fromMaybe)
import Control.Monad (forM_)
import GHC.Generics (Generic)
import Data.AffineSpace ((.-^))
import System.Locale (defaultTimeLocale)
import Data.Thyme
       (UTCTime, parseTime, getCurrentTime,
        NominalDiffTime, formatTime)
import Data.Thyme.Time.Core (fromThyme)
import Data.Text (Text)
import Data.Aeson (ToJSON(..), genericToJSON)
import Data.Aeson.Types (defaultOptions, Options(..), camelTo)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Log (MonadLog, WithTimestamp, WithSeverity)
import NSLogger (logNS, Severity(..))

import Database.MySQL.Simple
       (connect, query_, ConnectInfo(..), defaultConnectInfo, Connection, query)
import Database.MySQL.Simple.Result (Result(convert))
import Database.MySQL.Simple.QueryResults
       (QueryResults(..), convertError)

import Types


type SLType = Text
type SLEventType = Text

data LMSLogRow
    = ScormInteraction { siId :: Int
                       , siContentProgressID :: Maybe Int
                       , siInteraction :: Maybe Text}
    | SystemLogRow { slId :: !Int
                   , slUsersID :: Maybe Int
                   , slTimestamp :: Maybe Double
                   , slType :: Maybe SLType
                   , slTitle :: Maybe Text
                   , slContent :: Maybe Text
                   , slEventType :: Maybe SLEventType
                   , slCoursesID :: Maybe Int
                   , slLessonsID :: Maybe Int
                   , slSessionsID :: Maybe Int
                   , slBranchesID :: Maybe Int
                   , slInitiator :: Maybe Text
                   , slIpAddress :: Maybe Text
                   , slPhpSessionID :: Maybe Text}
    | CompletedTests { ctId :: Int
                     , ctTestsID :: Int
                     , ctStatus :: Maybe Text
                     , ctTimestamp :: Int
                     , ctArchive :: Int
                     , ctTimeStart :: Maybe Int
                     , ctTimeEnd :: Maybe Int
                     , ctTimeSpent :: Maybe Int
                     , ctScore :: Maybe Double
                     , ctPending :: Int
                     , ctUsersID :: Maybe Int
                     , ctContentProgressID :: Maybe Int
                     , ctDuration :: Maybe Int}
    deriving ((Generic))

instance ToJSON LMSLogRow where
    toJSON =
        genericToJSON
            (defaultOptions
             { fieldLabelModifier = camelTo '_' . drop 2
             })

instance QueryResults LMSLogRow where
    convertResults [f_id,f_content_progress_ID,f_interaction] [v_id,v_content_progress_ID,v_interaction] =
        ScormInteraction e_id e_content_progrress_ID e_interaction
      where
        !e_id = convert f_id v_id
        !e_content_progrress_ID =
            convert f_content_progress_ID v_content_progress_ID
        !e_interaction = convert f_interaction v_interaction
    convertResults [f_id,f_users_ID,f_timestamp,f_type,f_title,f_content,f_event_type,f_courses_ID,f_lessons_ID,f_sessions_ID,f_branches_ID,f_initiator,f_ip_address,f_php_session_id] [v_id,v_users_ID,v_timestamp,v_type,v_title,v_content,v_event_type,v_courses_ID,v_lessons_ID,v_sessions_ID,v_branches_ID,v_initiator,v_ip_address,v_php_session_id] =
        SystemLogRow
            e_id
            e_users_ID
            e_timestamp
            e_type
            e_title
            e_content
            e_event_type
            e_courses_ID
            e_lessins_ID
            e_sessions_ID
            e_branches_ID
            e_initiator
            e_ip_address
            e_php_session_id
      where
        !e_id = convert f_id v_id
        !e_users_ID = convert f_users_ID v_users_ID
        !e_timestamp = convert f_timestamp v_timestamp
        !e_type = convert f_type v_type
        !e_title = convert f_title v_title
        !e_content = convert f_content v_content
        !e_event_type = convert f_event_type v_event_type
        !e_courses_ID = convert f_courses_ID v_courses_ID
        !e_lessins_ID = convert f_lessons_ID v_lessons_ID
        !e_sessions_ID = convert f_sessions_ID v_sessions_ID
        !e_branches_ID = convert f_branches_ID v_branches_ID
        !e_initiator = convert f_initiator v_initiator
        !e_ip_address = convert f_ip_address v_ip_address
        !e_php_session_id = convert f_php_session_id v_php_session_id
    convertResults [f_id,f_tests_id,f_status,f_timestamp,f_archive,f_time_start,f_time_end,f_time_spent,f_score,f_pending,f_users_ID,f_content_progress_ID,f_duration] [v_id,v_tests_id,v_status,v_timestamp,v_archive,v_time_start,v_time_end,v_time_spent,v_score,v_pending,v_users_ID,v_content_progress_ID,v_duration] =
        CompletedTests
            e_id
            e_tests_id
            e_status
            e_timestamp
            e_archive
            e_time_start
            e_time_end
            e_time_spent
            e_score
            e_pending
            e_users_ID
            e_content_progress_ID
            e_duration
      where
        !e_id = convert f_id v_id
        !e_tests_id = convert f_tests_id v_tests_id
        !e_status = convert f_status v_status
        !e_timestamp = convert f_timestamp v_timestamp
        !e_archive = convert f_archive v_archive
        !e_time_start = convert f_time_start v_time_start
        !e_time_end = convert f_time_end v_time_end
        !e_time_spent = convert f_time_spent v_time_spent
        !e_score = convert f_score v_score
        !e_pending = convert f_pending v_pending
        !e_users_ID = convert f_users_ID v_users_ID
        !e_content_progress_ID =
            convert f_content_progress_ID v_content_progress_ID
        !e_duration = convert f_duration v_duration
    convertResults fs vs = convertError fs vs 2


lmsDefaultConnectInfo :: Host -> Login -> Password -> ConnectInfo
lmsDefaultConnectInfo host login pwd =
    defaultConnectInfo
    { connectHost = host
    , connectUser = login
    , connectPassword = pwd
    , connectDatabase = "efront"
    }

grabFromMySQLdb
    :: (MonadIO m, MonadLog (WithTimestamp (WithSeverity LMSLogRow)) m)
    => Host -> Login -> Password  -> NominalDiffTime -> m ()
grabFromMySQLdb host login pwd tr = do
    currentTime <- liftIO getCurrentTime
    conn <- liftIO $ connect (lmsDefaultConnectInfo host login pwd)
    loadFromMySQLdb conn (currentTime .-^ tr) currentTime


loadFromMySQLdb :: (MonadIO m, MonadLog (WithTimestamp (WithSeverity LMSLogRow)) m)
    => Connection -> UTCTime -> UTCTime  -> m ()
loadFromMySQLdb conn start finish = do
    xs <-
        liftIO $
        query
            conn
            "SELECT * FROM system_log WHERE timestamp BETWEEN ? AND ?;"
            (map utctime2timestamp [start, finish])
    forM_ xs $
        \slr ->
             logNS
                 (fromThyme
                      (fromMaybe start (timestamp2utctime =<< slTimestamp slr)))
                 Informational
                 slr
    cts <-
        liftIO $
        query
            conn
            "SELECT * FROM completed_tests WHERE timestamp BETWEEN ? AND ?;"
            (map utctime2timestamp [start, finish])
    forM_ cts $
        \ctr ->
             logNS
                 (fromThyme
                      (fromMaybe
                           start
                           (timestamp2utctime (fromIntegral (ctTimestamp ctr)))))
                 Informational
                 ctr





timestamp2utctime :: (RealFloat a) => a -> Maybe UTCTime
timestamp2utctime = parseTime defaultTimeLocale "%s%Q" . showRealFloat

utctime2timestamp :: UTCTime -> String
utctime2timestamp = formatTime defaultTimeLocale "%s%Q"

showRealFloat :: (RealFloat a) => a -> String
showRealFloat d = showFFloat Nothing d ""
