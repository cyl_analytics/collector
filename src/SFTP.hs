{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
module SFTP
    ( grabFromSftp
    , dateValue
    , lmsNginxLogRow
    ) where

import Prelude hiding (takeWhile)

import Data.Time
       (UTCTime, defaultTimeLocale, getCurrentTime, formatTime)
import System.Process
       (createProcess, proc, StdStream(CreatePipe), CreateProcess(..),
        showCommandForUser, CmdSpec(..))
import System.IO (Handle, hSetBinaryMode)
import Data.Word (Word8)
import Data.Text (Text)
import Data.Aeson (ToJSON)
import GHC.Generics (Generic)
import Data.ByteString (ByteString)
import Data.Map (Map)
import Data.Time (UTCTime, parseTimeM, defaultTimeLocale)
import Data.Attoparsec.ByteString
       (Parser, many1, takeByteString, sepBy', string,
        word8, takeTill, satisfy)
import Data.Attoparsec.ByteString (parse, eitherResult)
import Data.Attoparsec.ByteString.Char8 (decimal)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Log
       (MonadLog, WithSeverity(..), logMessage, WithTimestamp,
        Severity(..))
import NSLogger (logNS, Severity(..))


import qualified Data.Map as H
import qualified Data.Text.Encoding as TE
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Char8 as BSC8
import qualified Data.ByteString.UTF8 as BUTF8
import qualified Codec.Compression.GZip as GZip

import Types

import Debug.Trace


data LMSNginxLogRow = LMSNginxLogRow
    { getIP :: Text
    , getDate :: UTCTime
    , getRequestMethod :: Text
    , getRequestURL :: Text
    , getStatus :: Int
    , getBytes :: Int
    , getReferer :: Text
    , getUA :: Text
    , getPHPSSID :: Text
    , getCookies :: Map Text Text
    } deriving (Show, Generic)

instance ToJSON LMSNginxLogRow


digit :: Parser Word8
digit =
    satisfy
        (\w ->
              w >= 48 && w <= 57)

plainValue :: Parser ByteString
plainValue = takeTill (== 32)

quotedValue :: Parser ByteString
quotedValue = do
    word8 34
    value <- takeTill (== 34)
    word8 34
    return value

dateValue :: Parser UTCTime
dateValue = do
    word8 91
    date <-
        takeTill (== 93) >>=
        parseTimeM True defaultTimeLocale "%d/%b/%Y:%H:%M:%S %Z" .
        BUTF8.toString
    word8 93
    return date

requestValues :: Parser (ByteString, ByteString)
requestValues = do
    word8 34
    method <- plainValue
    word8 32
    url <- plainValue
    word8 32
    _ <- takeTill (== 34)
    word8 34
    return (method, url)

plainKeyValues :: Parser (Text, Text)
plainKeyValues = do
    key <- takeTill (== 61)
    word8 61
    value <- takeTill (\w -> w == 59 || w == 34)
    return (TE.decodeUtf8 key, TE.decodeUtf8 value)

cookiesValues :: Parser (Text, Map Text Text)
cookiesValues = do
    word8 34
    values <- plainKeyValues `sepBy'` string "; "
    word8 34
    let hmap = H.fromList values
    case H.lookup "PHPSESSID" hmap of
        Nothing -> fail "There is no PHPSESSID in cookies"
        Just val -> return (val, hmap)

lmsNginxLogRow :: Parser LMSNginxLogRow
lmsNginxLogRow = do
    ip <- plainValue
    word8 32
    _ <- plainValue
    word8 32
    _ <- plainValue
    word8 32
    date <- dateValue
    word8 32
    (method,url) <- requestValues
    word8 32
    status <- decimal
    word8 32
    bytes <- decimal
    word8 32
    referer <- quotedValue
    word8 32
    ua <- quotedValue
    word8 32
    (phpsessid,cookies) <- cookiesValues
    return $
        LMSNginxLogRow
            (TE.decodeUtf8 ip)
            date
            (TE.decodeUtf8 method)
            (TE.decodeUtf8 url)
            status
            bytes
            (TE.decodeUtf8 referer)
            (TE.decodeUtf8 ua)
            phpsessid
            cookies


generateUrl :: Host -> UTCTime -> String
generateUrl host ut =
    "sftp://" ++
    host ++
    "/nginx-logs/access-cookies-lms.startizen.ru.log-" ++
    (formatTime defaultTimeLocale "%Y%m%d" ut) ++ ".gz"

curlUserPwd :: Login -> Password -> String
curlUserPwd login pwd = login ++ ":" ++ pwd


-- | We call curl with -s, -k and -u parameters
-- see about parameters here: man curl
curlOpts :: Host -> Login -> Password -> UTCTime -> [String]
curlOpts host login pwd ut =
    ["-s", "-k", "-u", curlUserPwd login pwd, generateUrl host ut]

loadFromSftp :: Host -> Login -> Password -> IO Handle
loadFromSftp host login pwd = getCurrentTime >>= \utctime ->
    createProcess
        (cmd utctime)
        { std_out = CreatePipe
        } >>=
    \(_,Just hout,_,_) ->
         return hout
  where
    cmd ut = proc "/usr/bin/curl" (curlOpts host login pwd ut)

grabFromSftp
    :: (MonadIO m, MonadLog (WithTimestamp (WithSeverity LMSNginxLogRow)) m)
    => Host -> Login -> Password -> m ()
grabFromSftp host login pwd = do
    response <- liftIO (loadFromSftp host login pwd)
    liftIO $ hSetBinaryMode response True
    gzcontent <- liftIO $ BSL.hGetContents response
    let content = BSL.toStrict (GZip.decompress gzcontent)
    mapM_
        (\value ->
              case eitherResult (parse lmsNginxLogRow value) of
                  Left err -> return ()
                  Right log -> logNS (getDate log) Informational log)
        (BSC8.lines content)
