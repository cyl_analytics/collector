
module Types
    ( Host
    , Login
    , Password
    ) where

type Host = String
type Login = String
type Password = String
