{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import Data.Monoid ((<>))
import Control.Applicative ((<*>))
import Data.Yaml (ParseException(..))
import System.IO (stdout)
import NSLogger (renderNSLog)
import Data.Thyme (NominalDiffTime)

import qualified Options.Applicative as Options
import qualified Data.Yaml as Yaml
import qualified Data.ByteString.Lazy.Char8 as BLC8

import Types
import MySQLdb
import SFTP

import Control.Monad.Log



-- | Collector command line interface options
data COptions = COptions
    { config :: FilePath
    , grabMysql :: Bool
    , grabSftp :: Bool
    , timeRange :: NominalDiffTime
    }

data GeneralConfig = GeneralConfig
    { host :: Host
    , login :: Login
    , pwd :: Password
    }

type SFTPConfig = GeneralConfig
type MySQLConfig = GeneralConfig

-- | Collector config contains sftp and mysql configs
data CConfig = CConfig
    { sftpConfig :: SFTPConfig
    , mysqlConfig :: MySQLConfig
    }

-- | for parse from Yaml we parse from JSON :)
instance Yaml.FromJSON CConfig where
  parseJSON (Yaml.Object o) = do
    host <- o Yaml..: "host"
    mysql <- (o Yaml..: "mysql") >>= parseMySQL host
    sftp <- (o Yaml..: "sftp") >>= parseSFTP host
    return (CConfig sftp mysql)
      where
        parseMySQL h (Yaml.Object o) = do
          login_ <- o Yaml..: "login"
          password <- o Yaml..: "password"
          return (GeneralConfig h login_ password)

        parseSFTP h (Yaml.Object o) = do
          login_ <- o Yaml..: "login"
          password <- o Yaml..: "password"
          return (GeneralConfig h login_ password)


timeRangeOption :: Options.Parser NominalDiffTime
timeRangeOption =
    Options.option
        Options.auto
        (Options.long "timerange" <> Options.short 't' <> Options.metavar "T" <>
         Options.help "Grab data from T to now")

coptions :: Options.Parser COptions
coptions =
    COptions <$>
    Options.strOption
        (Options.long "config" <> Options.short 'c' <> Options.metavar "FILE" <>
         Options.help "Path to config file") <*>
    Options.switch
        (Options.long "grab-mysql" <>
         Options.help "When flag specified we grab from mysql") <*>
    Options.switch
        (Options.long "grab-sftp" <>
         Options.help "When flag specified we grab from sftp") <*>
    timeRangeOption


readConfig :: COptions -> IO (Either ParseException CConfig)
readConfig COptions{..} = Yaml.decodeFileEither config


catchOrRun :: Either ParseException CConfig -> NominalDiffTime -> Bool -> Bool -> IO ()
catchOrRun cconfig timeRange grabMysql grabSftp =
    case cconfig of
        Left err -> print err
        (Right (CConfig sftpConf mysqlConf)) -> do
            if grabMysql
                then (withBatchedHandler
                          defaultBatchingOptions
                          (mapM_ (BLC8.hPutStrLn stdout))
                          (\logToStdErr ->
                                runLoggingT
                                    (grabFromMySQLdb
                                         (host mysqlConf)
                                         (login mysqlConf)
                                         (pwd mysqlConf)
                                         timeRange)
                                    (logToStdErr . renderNSLog)))
                else return ()
            if grabSftp
                then (withBatchedHandler
                          defaultBatchingOptions
                          (mapM_ (BLC8.hPutStrLn stdout))
                          (\logToStdErr ->
                                runLoggingT
                                    (grabFromSftp
                                         (host sftpConf)
                                         (login sftpConf)
                                         (pwd sftpConf))
                                    (logToStdErr . renderNSLog)))
                else return ()



main :: IO ()
main = do
    cliargs <- Options.execParser opts
    cfg <- readConfig cliargs
    catchOrRun cfg (timeRange cliargs) (grabMysql cliargs) (grabSftp cliargs)
  where
    opts =
        Options.info
            (Options.helper <*> coptions)
            (Options.fullDesc <> Options.progDesc "Collect logs from LMS" <>
             Options.header
                 "collector - util for collect and transfer LMS logs to GCS")
